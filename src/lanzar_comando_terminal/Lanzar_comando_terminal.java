
package lanzar_comando_terminal;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 *
 * @author damt211
 */
public class Lanzar_comando_terminal {


    public static void main(String[] args) {
       String [] comando = {"cmd","/c", "ver"};
       Runtime rt = Runtime.getRuntime();
       
        try {
            Process p = rt.exec(comando);
            //OBTENGO UN STRING DE LECTURA DEL LA SALIDA ESTANDAR DEL PROCESO HIJO
            InputStream is = p.getInputStream();
            //CONVIERTO EL STREAM ANTERIOR UN CARACTER PARA PODER LEER CARACTER A CARACTER
            InputStreamReader isr = new InputStreamReader(is);
            //AÑADO EL STREAM ANTERIOR A UN BUFFER PARA PODER LEER LINEA A LINEA
            BufferedReader br = new BufferedReader(isr);
            
            System.out.println(br.readLine());
            System.out.println(br.readLine());
            
            //WHILE PARA LEER HASTA EL FINAL 
            String linea = br.readLine();
            while(linea!=null){
                System.out.println(linea);
                linea = br.readLine();
            }
            
            //CERRAMOS LOS STREAM
            br.close();
            
        } catch (IOException ex) {
            System.err.println(ex.getMessage());
        }
    }
    
}
